import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  public readonly linkedIn: string = 'linkedin.com/in/sandorduba';
  public readonly gitlab: string = 'gitlab.com/sandorduba';
  public readonly twitter: string = 'twitter.com/sandorduba';
  public readonly github: string = 'github.com/sandorduba';

  public readonly experienceYears: number;
  public readonly experienceMonths: number;

  constructor() {
    const experienceInDays = (Date.now() - +(new Date('2016.07.01'))) / (1000 * 60 * 60 * 24);
    this.experienceYears = Math.floor(experienceInDays / 365);
    console.log(this.experienceYears);
    console.log(((experienceInDays - this.experienceYears * 365) / 30));
    this.experienceMonths = Math.floor((experienceInDays - this.experienceYears * 365) / 30);
  }
}
