import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import { InfoContainerComponent } from './components/info-container/info-container.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [AboutComponent, InfoContainerComponent],
  imports: [
    CommonModule,
    AboutRoutingModule,
    SharedModule
  ]
})
export class AboutModule { }
