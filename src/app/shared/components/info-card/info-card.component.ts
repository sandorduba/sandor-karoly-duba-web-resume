import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent {
  public readonly linkedIn: string = 'linkedin.com/in/sandorduba';
  public readonly gitlab: string = 'gitlab.com/sandorduba';
  public readonly twitter: string = 'twitter.com/sandorduba';
  public readonly github: string = 'github.com/sandorduba';

  public readonly experienceYears: number;
  public readonly experienceMonths: number;

  constructor() {
    const experienceInDays = (Date.now() - +(new Date('2017.07.01'))) / (1000 * 60 * 60 * 24);
    this.experienceYears = Math.floor(experienceInDays / 365);
    this.experienceMonths = Math.floor((experienceInDays - this.experienceYears * 365) / 30);
  }
}
